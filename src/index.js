const {
	GetObjectCommand,
	ListObjectsCommand,
	PutObjectCommand,
	S3Client,
	HeadObjectCommand,
	DeleteObjectCommand,
} = require("@aws-sdk/client-s3");
const fs = require("fs");

class S3 {
	getDsn(_, dsn) {
		let matches = dsn.match(/^s3:\/\/(.*):(.*)@(.*):(.*)\/(.*)/);
		if (!matches) {
			throw "Bad format for aws dsn. Expected s3://KEY:SECRET@ENDPOINT:REGION/BUCKET";
		}
		let [__, accessKeyId, secretAccessKey, endpoint, region, bucket] = matches;
		const params = {
			credentials: { accessKeyId, secretAccessKey },
			bucket,
		};
		if (region != "") params.region = region;
		if (endpoint != "") params["endpoint"] = "https://" + endpoint;
		return params;
	}

	constructor(dsn) {
		let params = this.getDsn("s3", dsn);
		this.s3 = new S3Client(params);
		this.bucket = params.bucket;
	}

	readStream (stream) {
		return new Promise((resolve, reject) => {
			const chunks = [];
			stream.on("data", (chunk) => chunks.push(chunk));
			stream.on("error", reject);
			stream.on("end", () => resolve(Buffer.concat(chunks)));
		});
	}

	async getContent(key) {
		const response = await this.s3.send(new GetObjectCommand({ Bucket: this.bucket, Key: key }));
		return this.readStream(response.Body);
	}

	async get(key) {
		const response = await this.s3.send(new GetObjectCommand({ Bucket: this.bucket, Key: key }));
		return response.Body;
	}

	async list(opts) {
		const response = await this.s3.send(new ListObjectsCommand({ Bucket: this.bucket, ...opts }));
		return response.Contents;
	}

	async downloadFile(key, destPath) {
		const readStream = await this.get(key);
		const writeStream = fs.createWriteStream(destPath);
		return new Promise((resolve, reject) => {
			readStream.pipe(writeStream).on("finish", resolve).on("error", reject);
		});
	}

	async exist(key) {
		try {
			const res = await this.s3.send(new HeadObjectCommand({ Bucket: this.bucket, Key: key }));
			return true;
		} catch (error) {
			if (error.name != "NotFound") {
				throw error;
			}
		}
		return false;
	}

	upload(key, body, opts) {
		return this.s3.send(new PutObjectCommand({ Bucket: this.bucket, Key: key, Body: body, ...opts }));
	}

	uploadFile(key, path, opts) {
		const command = new PutObjectCommand({
			Bucket: this.bucket,
			Key: key,
			Body: fs.createReadStream(path),
			...opts,
		});
		return this.s3.send(command);
	}

	delete(key) {
		return this.s3.send(new DeleteObjectCommand({ Bucket: this.bucket, Key: key }));
	}
}

module.exports = S3;
