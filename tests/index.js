require("dotenv").config({ path: __dirname + "/../.env" });
const tap = require("tap");
const S3 = require("../src");
const fs = require("fs");

const s3 = new S3(process.env.S3_DSN);
const localPath = '/tmp/nodejs-s3-test-file';

tap.test("Test getDsn method", (t) => {
	try {
		const params = s3.getDsn("s3", "s3://accessKeyId:secretAccessKey@endpoint:region/bucket");
		t.ok(params, "Should return params");
	} catch (error) {
		t.fail(`Error: ${error}`);
	}
	t.end();
});

tap.test("Test constructor with invalid DSN", (t) => {
	try {
		new S3("invalid-dsn");
		t.fail("Should throw an error");
	} catch (error) {
		t.ok(error, "Should throw an error");
	}
	t.end();
});

tap.test("Test non-existing key", async (t) => {
	try {
		const exists = await s3.exist("some-key-not-existing");
		t.ok(exists === false);
	} catch (error) {
		t.fail(`Error: ${error}`);
	}
});

tap.test("Test exist method", async (t) => {
	try {
		const exists = await s3.exist("some-key");
		t.ok(typeof exists === "boolean", "Should return a boolean");
	} catch (error) {
		t.fail(`Error: ${error}`);
	}
});

tap.test("Test upload method", async (t) => {
	try {
		const result = await s3.upload("some-key", "some-data");
		t.ok(result, "Should return a result");
	} catch (error) {
		t.fail(`Error: ${error}`);
	}
});


tap.test("Test list method", async (t) => {
	try {
		const list = await s3.list();
		t.ok(list.length > 0, "Should return an array of length > 0");
	} catch (error) {
		t.fail(`Error: ${error}`);
	}
});

tap.test("Test get method", async (t) => {
	try {
		const stream = await s3.get("some-key");
		t.ok(stream, "Should return a stream");
	} catch (error) {
		t.fail(`Error: ${error}`);
	}
});

tap.test("Test getContent method", async (t) => {
	try {
		const content = await s3.getContent("some-key");
		t.ok(typeof(content.toString("utf-8") == "string"), "Should return a string");
	} catch (error) {
		t.fail(`Error: ${error}`);
	}
});

tap.test("Test downloadFile method", async (t) => {
	try {
		await s3.downloadFile("some-key", localPath);
		t.ok(true, "Should download the file without errors");
	} catch (error) {
		t.fail(`Error: ${error}`);
	}
});

tap.test("Test delete method", async (t) => {
	try {
		const result = await s3.delete("some-key");
		t.ok(result, "Should return a result");
	} catch (error) {
		t.fail(`Error: ${error}`);
	}
});

tap.test("Test uploadFile method", async (t) => {
	try {
		const result = await s3.uploadFile("some-key", localPath);
		t.ok(result, "Should return a result");
	} catch (error) {
		t.fail(`Error: ${error}`);
	}
});

// Test exist method with a non-existing key
tap.test("Test exist method with non-existing key", async (t) => {
	try {
		const exists = await s3.exist("non-existing-key");
		t.equal(exists, false, "Should return false");
	} catch (error) {
		t.fail(`Error: ${error}`);
	}
});

// Test get method with a non-existing key
tap.test("Test get method with non-existing key", async (t) => {
	try {
		await s3.get("non-existing-key");
		t.fail("Should throw an error");
	} catch (error) {
		t.ok(error, "Should throw an error");
	}
});

tap.teardown(async () => {
    try {
        fs.unlinkSync(localPath);
        console.log('Successfully removed local file');
    } catch (error) {
        console.error(`Failed to remove local file : ${error}`);
    }
});
